const express = require('express');
const router = express.Router();
const apiKey = 'AIzaSyCXqpjjc1uCWlKVMA5bSlvC73ZDX5RddZQ';
const eventApiKey = 'Z2LJLEEM62IZRN3VTCNL';
const meetupApiKey = '01359d71caf11633d41af1b370d924de'
const request = require('request');
const axios = require('axios');
const suburb = require('../../src/assets/suburbs.json');


// declare axios for making http requests

router.all("*", function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Headers", "content-type");

    res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);
    else
        next();
});

router.get('/suburb', (req, res) => {
    // console.log(suburb);
    res.json(suburb);
});

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

router.get('/point_of_interest/:poi', (req, res) => {
    let place = req.params.poi;
    //console.log("places" + place)
    getGoogleMapLocation(place).then(result => res.send(result));
});

router.get('/restaurant/:res', (req, res) => {
    let place = req.params.res;
    getGoogleMapLocation(place).then(result => res.send(result));
});

router.get('/event/:category', (req, res) => {
    let category = req.params.category;
    // console.log(category);
    getEvents(category).then(result => res.send(result));
})

router.get('/meetup/:category', (req, res) => {
    let category = req.params.category;
    getMeetupGroups(category).then(result => res.send(result));
})


async function getMeetupGroups(category) {
    const meetupSearchUrl = `https://api.meetup.com/find/groups?&sign=true&photo-host=public&text=${category}&zip=3000&status=active&country=australia&page=50&access_token=${meetupApiKey}`;
    return axios.get(meetupSearchUrl)
        .then(res => {
            let results = []
            for (let group of Object.values(res)[5]) {
                results.push({
                    'name': group['name'],
                    'members': group['members'],
                    'url': group['link'] ? group['link'] : null,
                    'image': group['key_photo'] ? group['key_photo']['photo_link'] : group['meta_category']['photo']['photo_link'],
                    'location': group['localized_location'],
                    'cat1': group['meta_category']['name'],
                    'cat2': group['category']['name']
                });
            }
            return results;
        }).catch(err => {
            console.log(`Error in searching groups: ${err}`);
        })
};


async function getEvents(category) {
    const eventSearchUrl = `https://www.eventbriteapi.com/v3/events/search/?q=festival&location.address=melbourne&categories=${category}&token=${eventApiKey}`;
    return axios.get(eventSearchUrl)
        .then(res => {
            let results = [];
            for (let event of Object.values(res['data']['events'])) {
                results.push({
                    'name': event['name']['text'],
                    'description': event['description']['text'],
                    'url': event.url ? event['url'] : null,
                    'image': event.logo ? event['logo']['url'] : null,
                    'start': event['start']['local'],
                    'end': event['end']['local']
                });
                // console.log(event['name']['text']);
            }
            return results;
        }).catch(err => {
            console.log(`Error in searching events: ${err}`);
        })
};

async function getGoogleMapLocation(place) {
    let placeIdList;
    let locations;
    try {
        placeIdList = await getPlaceIdList(place);
        locations = await getLocationByPlaceIds(placeIdList);
    } catch (e) {
        console.log(`Main function result: ${e}`);
    } finally {
        return locations;
    }
}

async function getPlaceIdList(place) {
    const restaurantSearchUrl = `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${place}}&language=en&key=${apiKey}`;
    return axios.get(restaurantSearchUrl)
        .then(res => {
            let placeIdList = [];
            let results = res['data']['results'];
            for (let result of results) {

                placeIdList.push({
                    'place_id': result['place_id'],
                    'name': result['name'],
                    'formatted_address': result['formatted_address'],
                    'photo_reference': result.photos ? "https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&maxheight=300&photoreference=" + result.photos[0]['photo_reference'] + "&key=" + apiKey : null,
                    'rating': result['rating']
                });
            }
            return placeIdList;
        })
        .catch(err => {
            console.log(`Error in searching result: ${err}`);
        })
}

async function getLocationByPlaceIds(placeIdList) {
    const placeLocateUrl = (placeId) => `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&fields=url&key=${apiKey}`;
    let requestList = placeIdList.map(e => axios.get(placeLocateUrl(e['place_id'])));

    return axios.all(requestList)
        .then(responseList => {
            return responseList.map((e, i) => ({
                'name': placeIdList[i]['name'],
                'formatted_address': placeIdList[i]['formatted_address'],
                'photo_reference': placeIdList[i]['photo_reference'],
                'rating': placeIdList[i]['rating'],
                'url': e['data']['result']['url'],
            }));
        })
        .catch(err => {
            console.log(`Error in fetching the location`);
        })
}

function doCall(urlToCall, callback) {
    request({ url, followRedirect: false }, function(err, res, body) {
        return callback(res.headers.location);
    });
}

/*
doCall(url, function(response) {
  res = response;
})
console.log(res)
*/


module.exports = router;

// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const https = require('https');
const bodyParser = require('body-parser');
const fs = require('fs')


// Get our API routes
const api = require('./server/routes/api');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist/heya-mate')));

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/heya-mate/index.html'));
});


const creds = {
    key: fs.readFileSync('ssl/privkey.pem'),
    cert: fs.readFileSync('ssl/cert.pem'),
    ca: fs.readFileSync('ssl/chain.pem')
};


/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '443';
app.set('port', port);

/**
 * Create HTTPS server.
 */
const server = https.createServer(creds, app);

/**
 * Listen on provided port, on all network interfaces.
 */
// <<<<<<< HEAD
server.listen(port, () => console.log(`API running on localhost:${port}`));
// =======
// server.listen(port, () => console.log(`API running on localhost:${port}`));
//
// >>>>>>> dev-ryan

http.createServer(function(req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80);
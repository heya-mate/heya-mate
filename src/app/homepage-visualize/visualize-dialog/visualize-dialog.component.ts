import {Component, Input, OnInit, Inject} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  visLink: string;
  visTitle: string;
}

@Component({
  selector: 'app-visualize-dialog',
  templateUrl: './visualize-dialog.component.html',
  styleUrls: ['./visualize-dialog.component.sass']
})
export class VisualizeDialogComponent implements OnInit {

 // @Input() visualisationUrl: string;
  visUrl: SafeResourceUrl = null;

  constructor(public dialogRef: MatDialogRef<VisualizeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.visUrl = this.trustUrl();
  }

  trustUrl() {
    if (this.data.visLink.length > 0) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.data.visLink);
    }
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeDialogComponent } from './visualize-dialog.component';

describe('VisualizeDialogComponent', () => {
  let component: VisualizeDialogComponent;
  let fixture: ComponentFixture<VisualizeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

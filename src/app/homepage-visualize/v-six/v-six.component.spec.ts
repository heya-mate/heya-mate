import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VSixComponent } from './v-six.component';

describe('VSixComponent', () => {
  let component: VSixComponent;
  let fixture: ComponentFixture<VSixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VSixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable, Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PreferenceService } from "../../services/preference.service";
declare var $: any;

@Component({
  selector: "app-restaurants",
  templateUrl: "./restaurants.component.html",
  styleUrls: ["./restaurants.component.sass"]
})
export class RestaurantsComponent implements OnInit {
  items: any[] = [];
  nationality: string;
  university: string = "";
  restaurantParemeter: string;
  showProgressSnipper = true;
  searchText: string;
  constructor(
    public http: HttpClient,
    public preferenceService: PreferenceService
  ) {}

  ngOnInit() {
    $(document).ready(function() {
      $("#toHome").show();
      $("#toPalette").hide();
    });

    this.onFetchRestaurants();
  }

  async onFetchRestaurants() {
    // chinese+restaurants+near+melbourne+australia
    // let restaurant = input + 'near+melbourne+australia';
    this.preferenceService.currentCountry.subscribe(
      country => (this.nationality = country)
    );
    if (typeof this.nationality === "undefined") {
      this.restaurantParemeter = "restaurants";
    } else {
      this.restaurantParemeter = this.nationality + " restaurants";
    }

    this.preferenceService.currentUniversity.subscribe(
      university => (this.university = university)
    );

    if (this.university.length <= 0) {
      this.restaurantParemeter =
        this.restaurantParemeter + " near Melbourne Australia";
    } else {
      this.restaurantParemeter =
        this.restaurantParemeter + " near " + this.university;
    }

    this.searchText = this.restaurantParemeter;
    this.http
      .get(`https://heya-mate.ml/api/restaurant/${this.restaurantParemeter}`)
      .subscribe(response => {
        const result = response;
        let numOfInterest = 0;
        for (let entry of Object.values(result)) {
          if (entry.formatted_address.length > 40) {
            entry.formatted_address =
              entry.formatted_address.substring(0, 40) + "...";
          }
          this.items.push({
            name: entry.name,
            rating: entry.rating,
            address: entry.formatted_address,
            photo_reference: entry.photo_reference,
            url: entry.url
          });
          numOfInterest++;
          if (numOfInterest === 50) {
            break;
          }
        }
        this.showProgressSnipper = false;
      });
  }

  async onSearchRestaurants(inputCountry: HTMLInputElement) {
    this.showProgressSnipper = true;
    if (inputCountry.value !== undefined) {
      this.restaurantParemeter =
        inputCountry.value + "+restaurants+near+melbourne+australia";
      this.items = [];
      this.http
        .get(`https://heya-mate.ml/api/restaurant/${this.restaurantParemeter}`)
        .subscribe(response => {
          const result = response;
          let numOfInterest = 0;
          for (let entry of Object.values(result)) {
            if (entry.formatted_address.length > 40) {
              entry.formatted_address =
                entry.formatted_address.substring(0, 40) + "...";
            }
            this.items.push({
              name: entry.name,
              rating: entry.rating,
              address: entry.formatted_address,
              photo_reference: entry.photo_reference,
              url: entry.url
            });
            numOfInterest++;
            if (numOfInterest === 6) {
              break;
            }
          }
          this.showProgressSnipper = false;
        });
    }
  }
  async onSearchRestaurantsKey(event, inputCountry: HTMLInputElement) {
    this.showProgressSnipper = true;
    if (inputCountry.value !== undefined) {
      this.restaurantParemeter =
        inputCountry.value + "+restaurants+near+melbourne+australia";
      this.items = [];
      this.http
        .get(`https://heya-mate.ml/api/restaurant/${this.restaurantParemeter}`)
        .subscribe(response => {
          const result = response;
          let numOfInterest = 0;
          for (let entry of Object.values(result)) {
            if (entry.formatted_address.length > 40) {
              entry.formatted_address =
                entry.formatted_address.substring(0, 40) + "...";
            }
            this.items.push({
              name: entry.name,
              rating: entry.rating,
              address: entry.formatted_address,
              photo_reference: entry.photo_reference,
              url: entry.url
            });
            numOfInterest++;
            if (numOfInterest === 6) {
              break;
            }
          }
          this.showProgressSnipper = false;
        });
    }
  }
}

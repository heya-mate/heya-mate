import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PreferenceService } from '../../services/preference.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SuburbsComponent} from '../suburbs/suburbs.component';
import {HttpClient} from '@angular/common/http';
import {FestivalsComponent} from "../festivals/festivals.component";

export interface PreferenceData {
  country: any;
  gender: any;
  issue: string;
  university: string;
}

@Component({
  selector: 'app-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.sass']
})
export class PreferenceComponent implements OnInit {

  form: FormGroup;
  country: string;
  gender: string;
  issue: string;
  university: string;

  countries: string[] = ["China", "India"]
  genders: string[] = ["Male", "Female", "Other"]
  issues: string[] = ["Culture Shock", "Loneliness & Anxiety", "Homesickness","Language & Translation" , "Other"]
  universities: string[] = ["Monash University (Caufield)", "Monash University (Clayton)", "University of Melbourne (Parkville)", "Other"]

  constructor(public fb: FormBuilder,
              public dialogRef: MatDialogRef<PreferenceComponent>,
              public preferenceService: PreferenceService,
              @Inject(MAT_DIALOG_DATA) public data: PreferenceData,
              public http: HttpClient) { }


  ngOnInit() {

    this.preferenceService.currentCountry.subscribe(country => this.country = country);
    this.preferenceService.currentGender.subscribe(gender => this.gender = gender);
    this.preferenceService.currentIssue.subscribe(issue => this.issue = issue);
    this.preferenceService.currentUniversity.subscribe(university => this.university = university);

    this.form = this.fb.group({
      country: [this.country, []],
      gender: [this.gender, []],
      issue: [this.issue, []],
      university: [this.university, []]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  setPreference() {
    this.preferenceService.setControlCountry(this.form.get("country").value);
    this.preferenceService.setControlGender(this.form.get("gender").value);
    this.preferenceService.setControlIssue(this.form.get("issue").value);
    this.preferenceService.setControlUniversity(this.form.get("university").value);
    // const suburbsComponent = new SuburbsComponent(this.http, this.preferenceService);
    // suburbsComponent.reloadMap();
    this.dialogRef.close();
  }
}

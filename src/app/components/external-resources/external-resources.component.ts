import { Component, OnInit } from '@angular/core';
import { PreferenceService } from '../../services/preference.service';


@Component({
  selector: 'app-external-resources',
  templateUrl: './external-resources.component.html',
  styleUrls: ['./external-resources.component.sass']
})
export class ExternalResourcesComponent implements OnInit {
  resources: string[][] = [
    ["Y", "Loneliness & Anxiety", "Lifeline Australia", "https://www.lifeline.org.au/", "LifelineAustralia.jpg", "Access 24/7 crisis support and suicide prevention services. Their charitable services are highly recommended if you're facing Anxiety, Depression, Loneliness or Stress."],
    ["Y", "Loneliness & Anxiety", "Monash Counselling", "https://www.monash.edu/health/counselling", "counselling.jpeg", "No issue is too big or too small and your university knows that. That is why you can visit the Monash counselling services to book a free appointment with a counselling professional. Moreover, based on your needs, you can see a counselor who is male, female or an Ally (trained in the needs of LGBTIQ people)."],
    ["Y", "Culture Shock", "Non-Residential Colleges", "https://www.monash.edu/non-residential-colleges", "p3.jpg", "If you don't live on campus and want to be a part of a student community which not only embraces different cultures but also values you as an individual, then you're most welcome to join in as a member of these colleges."],
    ["Y", "Culture Shock", "aMigo!", "https://amigo.apps.monash.edu/", "logo-aMigo.JPG", "aMigo is a brand new social network just for new Monash students that will help you connect with other students from all over the world who share the same interests as you."],
    ["Y", "Other", "Orygen", "https://www.orygen.org.au/", "orygen-logo-social.png", "Do you want to know about a research organisation whose aim is to minimize the mental health issues among youth? Go through this project, if you want to help someone in need."],
    ["Y", "Other", "Headspace", "https://www.headspace.org.au/", "logo-headspace.JPG", "More than 75 per cent of mental health issues develop before a person turns 25. Visit this site if you want to learn about mental health challenges and ways to maintain a healthy headspace."],
    ["Y", "Homesickness", "Dscribe", "http://www.dscribe.net.au/2018/09/16/homesick-life-begins-at-the-end-of-your-comfort-zone/", "logo-dscribe.JPG", "Watch and go through some of the tips that are provided from international students just like you, on how they managed to overcome their homesickness feelings."],
    ["Y", "Homesickness", "Discover English", "https://www.discoverenglish.com.au/blog/social/how-to-keep-homesickness-under-control", "logo-discover-english.JPG", "A really helpul blog, which provides tips that will prevent homesickness from stopping you from achieving your dreams."],
    ["Y", "Language & Translation", "Study Melbourne", "https://www.studymelbourne.vic.gov.au/help-and-support/study-melbourne-student-centre/support-services-at-smsc", "logo-study-melbourne.JPG", "Do you want to ask for help but feel less confident while approaching someone and communicating in English? Don't worry, Study Melbourne is an initiative taken up by the local government, which offers help in many languages other than English language. All the interpreting services are free of cost."],
    ["Y", "Language & Translation", "Monash English Connect", "https://www.monash.edu/english-connect", "En_connect.jpeg", "Free programs available to all Monash students so that they can develop their English communication skills. Helping you in making sure that language never becomes a barrier between you and your goals."]
  ]
  imagePath: string = "../../../assets/images/"
  constructor(public preferenceService: PreferenceService) { }

  issue: string;
  catFilter: string[];
  catFilters: string[] = ["All","Culture Shock", "Loneliness & Anxiety", "Homesickness","Language & Translation", "Other"]

  ngOnInit() {
    this.preferenceService.currentIssue.subscribe(issue => {
      this.issue = issue
      if (this.issue.length === 0) {
        var i = 0;
        for (let res of this.resources) {
          this.resources[i][0] = "Y"
          i++;
        }
      }
      else {
        var i = 0;
        for (let res of this.resources) {
          if (Object.values(this.issue).indexOf(res[1]) === -1) {
            this.resources[i][0] = "N"
          }
          else { this.resources[i][0] = "Y" }
          i++;
        }
      }
    });
  }

  onFilter(filter){
    this.catFilter = filter

    if ((this.catFilter.length === 0) || (this.catFilter.indexOf("All") != -1)){
      var i = 0;
      for (let res of this.resources) {
        this.resources[i][0] = "Y"
        i++;
      }
    }
    else {
      var i = 0;
      for (let res of this.resources) {
        if ((this.catFilter).indexOf(res[1]) === -1) {
          this.resources[i][0] = "N"
        }
        else { this.resources[i][0] = "Y" }
        i++;
      }
    }
  }
}

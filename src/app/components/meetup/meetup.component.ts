import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PreferenceService } from "../../services/preference.service";
declare var $: any;

@Component({
  selector: "app-meetup",
  templateUrl: "./meetup.component.html",
  styleUrls: ["./meetup.component.sass"]
})
export class MeetupComponent implements OnInit {
  items: any[] = [];
  issue: string;
  description: string;
  meetup_search: string;
  showProgressSnipper = true;
  constructor(
    public http: HttpClient,
    public preferenceService: PreferenceService
  ) {}

  ngOnInit() {
    $(document).ready(function() {
      $("#toHome").show();
      $("#toPalette").hide();
    });

    this.onFetchGroups();
  }

  async onFetchGroups() {
    this.showProgressSnipper = true;
    this.preferenceService.currentIssue.subscribe(
      issue => (this.issue = issue)
    );
    if (typeof this.issue === "undefined") {
      this.description = "mental health support groups";
    } else {
      this.description = "mental health support groups";
    }
    this.description = "mental health support groups";
    this.meetup_search = this.description;
    this.http
      .get(`https://heya-mate.ml/api/meetup/${this.description}`)
      .subscribe(response => {
        const result = response;
        let numOfInterest = 0;
        for (let entry of Object.values(result)) {
          if (entry.location.length > 40) {
            entry.location = entry.location.substring(0, 40) + "...";
          }
          console.log(entry.url);
          this.items.push({
            name: entry.name,
            members: entry.members,
            location: entry.location,
            image: entry.image,
            url: entry.url,
            category: entry.cat1 + " " + entry.cat2
          });
          numOfInterest++;
          if (numOfInterest === 50) {
            break;
          }
        }
        this.showProgressSnipper = false;
      });
  }

  async onSearchGroups(inputIssue: HTMLInputElement) {
    this.showProgressSnipper = true;
    if (inputIssue.value !== undefined) {
      this.description = inputIssue.value;
      this.items = [];
      this.http
        .get(`https://heya-mate.ml/api/meetup/${this.description}`)
        .subscribe(response => {
          const result = response;
          let numOfInterest = 0;
          for (let entry of Object.values(result)) {
            if (entry.location.length > 40) {
              entry.location = entry.location.substring(0, 40) + "...";
            }
            this.items.push({
              name: entry.name,
              members: entry.members,
              location: entry.location,
              image: entry.image,
              url: entry.url,
              category: entry.cat1 + " " + entry.cat2
            });
            numOfInterest++;
            if (numOfInterest === 6) {
              break;
            }
          }
          this.showProgressSnipper = false;
        });
    }
  }
}

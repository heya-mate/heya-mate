import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { CorePopupComponent } from '../core-popup/core-popup.component';
import { CorePopupService } from '../../services/core-popup.service';


@Component({
  selector: 'app-core-functions',
  templateUrl: './core-functions.component.html',
  styleUrls: ['./core-functions.component.sass']
})
export class CoreFunctionsComponent implements OnInit {

  flag = true; // true: hide popup. false: display popup.

  constructor(public dialog: MatDialog, public corePopupService: CorePopupService) { }

  ngOnInit() {
  }

  display() {
    this.flag = false;
  }

  hidden() {
    this.flag = true;
  }

  openPopup(cs: boolean,hs:boolean,la:boolean): void {

    this.corePopupService.setControlCultureShock(cs);
    this.corePopupService.setControlHomeSick(hs);
    this.corePopupService.setControlLoneAnx(la);
    
   const dialogRef = this.dialog.open(CorePopupComponent ,{
      hasBackdrop: true,
      width: "90vw",
      ariaDescribedBy: "core-popup",
    });


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreFunctionsComponent } from './core-functions.component';

describe('CoreFunctionsComponent', () => {
  let component: CoreFunctionsComponent;
  let fixture: ComponentFixture<CoreFunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoreFunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoreFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

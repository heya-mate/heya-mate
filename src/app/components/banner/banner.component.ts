import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
declare var $: any;
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { PreferenceComponent } from "../preference/preference.component";
import { PreferenceService } from "../../services/preference.service";

@Component({
  selector: "app-banner",
  templateUrl: "./banner.component.html",
  styleUrls: ["./banner.component.sass"],
  encapsulation: ViewEncapsulation.None
})
export class BannerComponent implements OnInit {
  country: string;
  gender: string;
  issue: string;
  university: string;
  badgeCount: number;
  badgeHidden: boolean = false;

  flag = true; // true: hide popup. false: display popup.

  constructor(
    public dialog: MatDialog,
    public preferenceService: PreferenceService,
    private router: Router
  ) {}

  ngOnInit() {

    $(document).ready(function() {
      $(window).scroll(function() {
        $("#header").css("opacity", 1 - $(window).scrollTop() / 100);
      });

      $("#header").mousemove(function() {
        $("#header").css("opacity", 1);
      });

      $("#toTop").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
      });
    });

    this.preferenceService.currentCountry.subscribe(
      country => (this.country = country)
    );
    this.preferenceService.currentGender.subscribe(
      gender => (this.gender = gender)
    );
    this.preferenceService.currentIssue.subscribe(
      issue => (this.issue = issue)
    );
    this.preferenceService.currentUniversity.subscribe(
      university => (this.university = university)
    );

    if (this.country.length > 0) {
      this.badgeCount++;
    }
    if (this.gender.length > 0) {
      this.badgeCount++;
    }
    if (this.issue.length > 0) {
      this.badgeCount++;
    }
    if (this.university.length > 0) {
      this.badgeCount++;
    }
    if (this.badgeCount > 0) {
      this.badgeHidden = false;
    } else {
      this.badgeHidden = true;
    }
  }

  openPreference(): void {
    const dialogRef = this.dialog.open(PreferenceComponent, {
      hasBackdrop: true,
      ariaDescribedBy: "preference"
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
    });
  }

  display() {
    this.flag = false;
  }

  hidden() {
    this.flag = true;
  }

  openPage(pageName): void {
    this.router.navigate(["/" + pageName]).then(e => {
      if (e) {
        console.log("Events Navigation is successful!");
      } else {
        console.log("Events Navigation has failed!");
      }
    });
  }
}

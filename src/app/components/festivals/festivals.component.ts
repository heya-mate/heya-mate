import { Component, OnInit } from '@angular/core';
import { PreferenceService } from '../../services/preference.service';
declare var $: any;
@Component({
  selector: 'app-festivals',
  templateUrl: './festivals.component.html',
  styleUrls: ['./festivals.component.sass']
})
export class FestivalsComponent implements OnInit {

  nationality;

  items: any[];

  allFestival: any[];
  indianFestival: any[];
  chineseFestival: any[];

  optionsSelect: Array<any>;

  constructor(public preferenceService: PreferenceService) {

    this.preferenceService.currentCountry.subscribe(country => this.nationality = country);

    this.optionsSelect = [
      { value: 'none', label: 'All'},
      { value: 'india', label: 'Indian festivals' },
      { value: 'china', label: 'Chinese festivals' }
    ];

    this.allFestival = [{
      name: 'Lunar New Year Festivals 2020',
      image: 'https://www.onlymelbourne.com.au/images4/0320-yorat.jpg',
      link: 'https://www.onlymelbourne.com.au/melbourne-lunar-new-year-festivals#.XaF4y-czZQY',
      description: 'Lunar New Year festivals in the CBD and suburbs.'
    },{
      name: 'International Dumpling Festival',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSeHMcN1M90zn1glBRDX6ovmSLJQ6sWtgehNAsMxH1wh67MgGGUeoVOXQWzQAL0rlPlTQ1eLYwhH8w&s',
      link: 'https://www.weekendnotes.com/melbourne-international-dumpling-festival/',
      description: 'it\'s a good excuse to celebrate and give your taste buds the excitement.'
    },{
      name: 'Diwali Indian Festival of Light 2019',
      image: 'https://www.onlymelbourne.com.au/images4/1019-diwalifedsq.jpg',
      link: 'https://www.onlymelbourne.com.au/diwali-indian-festival-of-light',
      description: 'Enjoy and experience the traditional and contemporary cultural feast...'
    },{
      name: 'Victorian Festival of Diwali',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvuASxoW0uW4K9TN2Sh0DmOT0YFKFn7tpViUKGABWhcdadAZDhSEOcvNc3r7Zp7fRG7cNqVtkW0B4',
      link: 'https://whatson.melbourne.vic.gov.au/whatson/festivals/multicultural/pages/06f6aa8a-2518-47dd-9ba5-acc48a7fc396.aspx',
      description: 'Celebrate Diwali, a breathtaking Indian cultural feast of dance, music...'
    },{
      name: 'Indian Attire Bollywood Diwali after party',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQl8HXte1o-A0lx3JJp_S8m-llsRI95w7LJ8vgOeAcZDf5mn460T-YXbKC-&s=10',
      link: 'https://melbourne.eventful.com/events/indian-attire-bollywood-diwali-after-party-/E0-001-129585693-9',
      description: 'This Diwali get ready to dance to Bollywood songs in your indian outfits!!'
    },{
      name: 'Diwali Fireworks Cruise',
      image: 'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F68199397%2F325153298329%2F1%2Foriginal.20190810-052726?w=800&auto=compress&rect=0%2C41%2C500%2C250&s=062a391d64daa1156d4d0e72bcdd553c',
      link: 'https://www.eventbrite.com.au/e/diwali-cruise-with-fireworks-tickets-68664915611',
      description: 'This will be hottest party in town SO DON\'T MISS IT!'
    }];

    this.chineseFestival = [{
      name: 'Lunar New Year Festivals 2020',
      image: 'https://www.onlymelbourne.com.au/images4/0320-yorat.jpg',
      link: 'https://www.onlymelbourne.com.au/melbourne-lunar-new-year-festivals#.XaF4y-czZQY',
      description: 'Lunar New Year festivals in the CBD and suburbs.'
    }];

    this.indianFestival = [{
      name: 'Diwali Indian Festival of Light 2019',
      image: 'https://www.onlymelbourne.com.au/images4/1019-diwalifedsq.jpg',
      link: 'https://www.onlymelbourne.com.au/diwali-indian-festival-of-light',
      description: 'Enjoy and experience the traditional and contemporary cultural feast...'
    },{
      name: 'Victorian Festival of Diwali',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvuASxoW0uW4K9TN2Sh0DmOT0YFKFn7tpViUKGABWhcdadAZDhSEOcvNc3r7Zp7fRG7cNqVtkW0B4',
      link: 'https://whatson.melbourne.vic.gov.au/whatson/festivals/multicultural/pages/06f6aa8a-2518-47dd-9ba5-acc48a7fc396.aspx',
      description: 'Celebrate Diwali, a breathtaking Indian cultural feast of dance, music...'
    },{
      name: 'Indian Attire Bollywood Diwali after party',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQl8HXte1o-A0lx3JJp_S8m-llsRI95w7LJ8vgOeAcZDf5mn460T-YXbKC-&s=10',
      link: 'https://melbourne.eventful.com/events/indian-attire-bollywood-diwali-after-party-/E0-001-129585693-9',
      description: 'This Diwali get ready to dance to Bollywood songs in your indian outfits!!'
    },{
      name: 'Diwali Fireworks Cruise',
      image: 'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F68199397%2F325153298329%2F1%2Foriginal.20190810-052726?w=800&auto=compress&rect=0%2C41%2C500%2C250&s=062a391d64daa1156d4d0e72bcdd553c',
      link: 'https://www.eventbrite.com.au/e/diwali-cruise-with-fireworks-tickets-68664915611',
      description: 'This will be hottest party in town SO DON\'T MISS IT!'
    }];
  }

  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(document).ready(function() {
      $('#toHome').show();
      $('#toPalette').hide();

});

    this.preferenceService.currentCountry.subscribe(country => {
      this.nationality = country;
      if (this.nationality === 'India') {
        this.items = this.indianFestival;
      } else if (this.nationality === 'China') {
        this.items = this.chineseFestival;
      } else {
        this.items = this.allFestival;
      }
    });
  }

  onOptionSelected(value) {
    if (value === 'india') {
      this.items = this.indianFestival;
    } else if (value === 'china') {
      this.items = this.chineseFestival;
    } else if (value === 'none') {
      this.items = this.allFestival;
    }
  }
}

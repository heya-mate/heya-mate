import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
declare var $: any;

@Component({
  selector: 'app-slang',
  templateUrl: './slang.component.html',
  styleUrls: ['./slang.component.sass']
})
export class SlangComponent implements OnInit {
  slangs: string[] = ['Slang-o-nyms', 'The Quiz'];
  slangChoice: string = this.slangs[0];

  displayedColumns: string[] = ['slang', 'value'];
  dataSource = new MatTableDataSource(SLANG_DATA);

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(document).ready(function() {
      $('#toHome').show();
      $('#toPalette').hide();

});

    this.dataSource.sort = this.sort;
  }

}

export interface SlangList {
  slang: string;
  value: string;
}

const SLANG_DATA: SlangList[] = [
  {slang:'Ace',                   value: 'Great'},
  {slang:'Ankle-biter',           value: 'Child'},
  {slang:'Arvo',                  value: 'Evening '},
  {slang:'Barbie',                value: 'Barbeque'},
  {slang:'Bloke',                 value: 'Male'},
  {slang:'Bludger',               value: 'Lazy Person'},
  {slang:'Buggered',              value: 'Tired'},
  {slang:'Dunny',                 value: 'Toilet'},
  {slang:'Hot under the collar',  value: 'Get angry'},
  {slang:'Have a blue',           value: 'Have a fight'},
  {slang:'Idiot box',             value: 'Television'},
  {slang:'Maccas',                value: 'McDonalds'},
  {slang:'Call it a day',         value: 'Finish what you are doing '},
  {slang:'Fair go ',              value: 'Give soemone an equal chance'},
  {slang:'Give someone a bell',   value: 'Call someone on the phone'},
  {slang:'Good on ya',            value: 'Good for you'},
  {slang:'Jumper',                value: 'Sweater'},
  {slang:'Don\'t Knock It',       value: 'Don\'t criticise it'},
  {slang:'Mucking around ',       value: 'Being foolish'},
  {slang:'No worries',            value: 'No problem'},
  {slang:'Rip off',               value: 'To cheat / Very expensive'},
  {slang:'Rug up ',               value: 'Dress warmly'},
  {slang:'Slack',                 value: 'Lazy'},
  {slang:'Slab',                  value: 'Carton of beer'},
  {slang:'Wuss',                  value: 'A coward'},
  {slang:'Veggo',                 value: 'A vegetarian'},
  {slang:'Ta',                   value: 'Thank you'},
  {slang:'Sunnies',               value: 'Sunglasses'},
  {slang:'Spud',                  value: 'A potato'},
  {slang:'She\'ll be right',      value: 'It\'ll be fine'},
  {slang:'Servo',                 value: 'Service Station'},
  {slang:'A cold one',            value: 'Cold beer'},
  {slang:'Bathers',               value: 'Swimsuit'},
  {slang:'Brolly',                value: 'Umbrella'},
  {slang:'Chook',                 value: 'Chicken'},
  {slang:'Straya',                value: 'Australia'},
  {slang:'Pissed',                value: 'Drunk'},
  {slang:'Piece of Piss',         value: 'Easy'},
  {slang:'Lollies',               value: 'Sweets'},
  {slang:'Lappy',                 value: 'Laptop'},
  {slang:'Heaps',                 value: 'Many'},
  {slang:'Gnaly',                 value: 'Awesome'},
  {slang:'G\'day',                value: 'Hello'},
  {slang:'Nuddy',                 value: 'Naked'},
  {slang:'Pissed off',            value: 'Annoyed'},
  {slang:'Snag',                  value: 'Sausage'},
  {slang:'Facey',                 value: 'Facebook'},
  {slang:'Coppers',               value: 'Policemen'},
  {slang:'Cactus',                value: 'Dead'},
  {slang: 'Bloody oath',          value: 'yes or its true'},
  {slang: 'Bottle-O',             value: 'A place to buy alcohol to take home'},
  {slang: 'Bail',                 value: 'To cancel plans. "I\'m going to bail"'},
  {slang: 'Choccas',              value: 'Full'},
  {slang: 'Crack the shits',      value: 'Get angry at someone or something'},
  {slang: 'Dag',                  value: 'Someone who’s a bit of a nerd or geek.'},
  {slang: 'Daks/ Tracky daks',    value: 'Underwear/ Sweatpants'},
  {slang: 'Esky',                 value: 'Insulated container that keeps things cold'},
  {slang: 'Servo',                value: 'Fuel stop/ Service Station'},
  {slang: 'Stubby' ,              value: 'a bottle of beer'},
  {slang: 'Tinny',                value: 'A Can of beer'},
  {slang: 'Sickie',               value: 'Take a day off work when you aren’t actually sick'},
];

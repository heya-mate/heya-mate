import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlangComponent } from './slang.component';

describe('SlangComponent', () => {
  let component: SlangComponent;
  let fixture: ComponentFixture<SlangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { PreferenceService } from '../../services/preference.service';
// import {toTitleCase} from "codelyzer/util/utils";
declare var $: any;
declare let L;

@Component({
  selector: 'app-suburbs',
  templateUrl: './suburbs.component.html',
  styleUrls: ['./suburbs.component.sass']
})
export class SuburbsComponent implements OnInit {

  university: string;
  nationality: string;

  constructor(public http: HttpClient, public preferenceService: PreferenceService) { }

  ngOnInit() {

    $(document).ready(function() {
      $('#toHome').show();
      $('#toPalette').hide();

});
    this.initialMap();
  }

  initialMap() {
    L.DomUtil.get('mapid')._leaflet_id = null;
    const legend = L.control({position: 'topright', height: '80px'});
    const littleton = L.marker([-37.907803, 145.133957]).bindPopup('Monash Clayton Campus');
    const denver = L.marker([-37.877640, 145.043480]).bindPopup('Monash Caufield Campus');
    const aurora = L.marker([-37.876823, 145.045837]).bindPopup('Monash Peninsula Campus');
    const golden = L.marker([-37.7840, 144.9587]).bindPopup('Monash Parkville Campus');
    let mymap;

    this.preferenceService.currentUniversity.subscribe(university => this.university = university);
    this.preferenceService.currentCountry.subscribe(country => this.nationality = country);

    if (typeof this.university === "undefined") {
      mymap = L.map('mapid').setView([-37.877640, 145.043480], 10);
    } else {
      mymap = L.map('mapid', golden).setView([-37.877640, 145.043480], 10);

      switch (this.university) {
        case 'Monash University (Caufield)' : {
          L.marker([-37.907803, 145.133957]).bindPopup('Monash University (Caufield)').addTo(mymap);
          break;
        }

        case 'Monash University (Clayton)' : {
          L.marker([-37.907803, 145.133957]).bindPopup('Monash University (Clayton)').addTo(mymap);
          break;
        }

        case 'University of Melbourne (Parkville)' : {
          L.marker([-37.7840, 144.9587]).bindPopup('Monash University (Parkville)').addTo(mymap);
          break;
        }

        default : { break; }
      }



    }

    legend.onAdd = (mymap) => {

      const div = L.DomUtil.create('div', 'info legend');
      const countries = ['China', 'India'];
      const colors = ['#ff0000', '#3366ff'];

      // loop through our density intervals and generate a label with a colored square for each interval
      for (let i = 0; i < countries.length; i++) {
        div.innerHTML += `<i style="background:${colors[i]}; width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7 "></i> ` +
          countries[i] + '<br>';
      }

      div.setAttribute('style', 'background-color: #F9F5F5; border: 1px solid #555; line-height: 18px; opacity: 0.7')

      return div;
    };

    legend.addTo(mymap);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYnR0b2wiLCJhIjoiY2sxMzNiMmJrMDViZTNvbHphYzc0eWRzZyJ9.DSIh50JTIJxup2Q5ND4mbw', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.light',
      accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);

    this.http.get('https://heya-mate.ml/api/suburb').subscribe((response) => {

      // console.log(response);
      let sub = [];

      if (this.nationality === 'China') {
        for (let item of response['features']) {
          if (item.properties.vic_loca_3 === 'China') {
            sub.push(item);
          }
          // console.log(item.properties.vic_loca_3);
        }
      } else if (this.nationality === 'India') {
        for (let item of response['features']) {
          if (item.properties.vic_loca_3 === 'India') {
            sub.push(item);
          }
        }
      } else {
        for (let item of response['features']) {
            sub.push(item);
        }
      }

      L.geoJson(sub, {
        style: feature => ({'color': countryStyle[feature['properties']['vic_loca_3']]}),
        onEachFeature: onEachFeature
      }).addTo(mymap);

    });

    const countryStyle = {
      'India': '#3366ff',
      'China': '#ff0000',
    }

    function onEachFeature(feature, layer) {
      let suburbName = feature['properties']['vic_loca_2'].toLowerCase();
      let upperCaseSuburb = suburbName.charAt(0).toUpperCase() + suburbName.substring(1);
      layer.bindPopup(`<h4 style="font-weight: bold; margin-top: 10px">Suburb: ${upperCaseSuburb}</h4>
                       <b style="font-size: 15px; margin-bottom: 3px">PostCode: ${feature['properties']['vic_loca_1']}</b>
                       <br>
                       <a href= "https://www.realestate.com.au/rent/in-${upperCaseSuburb}+ vic +${feature['properties']['vic_loca_1']}/map-1?channel=rent&source=location-search" target="_blank"><button class="btn" style="border: 1px solid #091E5B; padding: 2px; margin-top: 5px">Rent a property</button></a>`);

      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
      });
    }

    function highlightFeature(e) {
      const layer = e.target;

      layer.setStyle({
        weight: 5,
        dashArray: '',
        fillOpacity: 0.5
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }
    }

    function resetHighlight(e) {
      const layer = e.target;

      layer.setStyle({
        weight: 3,
        dashArray: null,
        fillOpacity: 0.2
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }

    }

    function zoomToFeature(e) {
      mymap.fitBounds(e.target.getBounds());
    }
  }

  reloadMap(inputIssue: HTMLInputElement) {

    let isNotExist = true;

    if (L.DomUtil.get('mapid') !== null) {
      L.DomUtil.get('mapid')._leaflet_id = null;
    const legend = L.control({position: 'topright', height: '80px'});
    const littleton = L.marker([-37.907803, 145.133957]).bindPopup('Monash Clayton Campus');
    const denver = L.marker([-37.877640, 145.043480]).bindPopup('Monash Caufield Campus');
    const aurora = L.marker([-37.876823, 145.045837]).bindPopup('Monash Peninsula Campus');
    const golden = L.marker([-37.7840, 144.9587]).bindPopup('Monash Parkville Campus');
    let mymap = L.map('mapid').setView([-37.877640, 145.043480], 10);

    this.preferenceService.currentUniversity.subscribe(university => this.university = university);

    if (typeof this.university === "undefined" || this.university === "Other") {
      L.DomUtil.get('mapid')._leaflet_id = null;
      mymap = L.map('mapid').setView([-37.877640, 145.043480], 10);
    } else {

      L.DomUtil.get('mapid')._leaflet_id = null;

      switch (this.university) {
        case 'Monash University (Caufield)' : {
          mymap = L.map('mapid').setView([-37.877640, 145.043480], 11);
          L.marker([-37.907803, 145.133957]).bindPopup('Monash University (Caufield)').addTo(mymap);
          break;
        }

        case 'Monash University (Clayton)' : {
          mymap = L.map('mapid').setView([-37.907803, 145.133957], 11);
          L.marker([-37.907803, 145.133957]).bindPopup('Monash University (Clayton)').addTo(mymap);
          break;
        }

        case 'University of Melbourne (Parkville)' : {
          mymap = L.map('mapid').setView([-37.7840, 144.9587], 11);
          L.marker([-37.7840, 144.9587]).bindPopup('Monash University (Parkville)').addTo(mymap);
          break;
        }

        default : { break; }
      }

    }

    legend.onAdd = (mymap) => {

      const div = L.DomUtil.create('div', 'info legend');
      const countries = ['China', 'India'];
      const colors = ['#ff0000', '#3366ff'];

      // loop through our density intervals and generate a label with a colored square for each interval
      for (let i = 0; i < countries.length; i++) {
        div.innerHTML += `<i style="background:${colors[i]}; width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7 "></i> ` +
          countries[i] + '<br>';
      }

      div.setAttribute('style', 'background-color: #F9F5F5; border: 1px solid #555; line-height: 18px; opacity: 0.7')

      return div;
    };

    legend.addTo(mymap);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYnR0b2wiLCJhIjoiY2sxMzNiMmJrMDViZTNvbHphYzc0eWRzZyJ9.DSIh50JTIJxup2Q5ND4mbw', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.light',
      accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);

    this.http.get('https://heya-mate.ml/api/suburb').subscribe((response) => {

      let sub = [];

      for (let item of response['features']){
        if (item.properties.vic_loca_2.toUpperCase() === inputIssue.value.toUpperCase()) {
          console.log(item.properties.vic_loca_2);
          console.log(inputIssue.value.toUpperCase());
          sub.push(item);
          isNotExist = false;
        }
      }

      if (isNotExist){
        for (let item of response['features']){
            sub.push(item);
          }
        alert('The suburb you searched for does not exist!');
      }

      L.geoJson(sub, {
        style: feature => ({'color': countryStyle[feature['properties']['vic_loca_3']]}),
        onEachFeature: onEachFeature
      }).addTo(mymap);

    });

    const countryStyle = {
      'India': '#3366ff',
      'China': '#ff0000',
    }

    function onEachFeature(feature, layer) {
      let suburbName = feature['properties']['vic_loca_2'].toLowerCase();
      let upperCaseSuburb = suburbName.charAt(0).toUpperCase() + suburbName.substring(1);
      layer.bindPopup(`<h4>Suburb: ${upperCaseSuburb}</h4>
                       <b>PostCode: ${feature['properties']['vic_loca_1']}</b>
                       <br>
                       <a href= "https://www.realestate.com.au/rent/in-${upperCaseSuburb}+ vic +${feature['properties']['vic_loca_1']}/map-1?channel=rent&source=location-search" target="_blank"><button style="padding: 0">Rent a house</button></a>`);

      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
      });
    }

    function highlightFeature(e) {
      const layer = e.target;

      layer.setStyle({
        weight: 5,
        dashArray: '',
        fillOpacity: 0.5
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }
    }

    function resetHighlight(e) {
      const layer = e.target;

      layer.setStyle({
        weight: 3,
        dashArray: null,
        fillOpacity: 0.2
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }

    }

    function zoomToFeature(e) {
      mymap.fitBounds(e.target.getBounds());
    }
    }
  }
}

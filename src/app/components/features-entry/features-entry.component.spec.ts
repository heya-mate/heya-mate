import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesEntryComponent } from './features-entry.component';

describe('FeaturesEntryComponent', () => {
  let component: FeaturesEntryComponent;
  let fixture: ComponentFixture<FeaturesEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturesEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

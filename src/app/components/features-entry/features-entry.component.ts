import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-features-entry',
  templateUrl: './features-entry.component.html',
  styleUrls: ['./features-entry.component.sass']
})
export class FeaturesEntryComponent implements OnInit {

  constructor(private router: Router) { }

  openPage(pageName): void {
    this.router.navigate(["/"+pageName]).then((e) => {
      if (e) {
        console.log("Events Navigation is successful!");
      } else {
        console.log("Events Navigation has failed!");
      }
    });
  }

  ngOnInit() {
  }

}

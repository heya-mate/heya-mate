import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
declare var $: any;

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.sass"]
})
export class FooterComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  openPage(pageName): void {
    this.router.navigate(["/" + pageName]).then(e => {
      if (e) {
        console.log("Events Navigation is successful!");
      } else {
        console.log("Events Navigation has failed!");
      }
    });
  }
}

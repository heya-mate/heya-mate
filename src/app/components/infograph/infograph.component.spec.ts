import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfographComponent } from './infograph.component';

describe('InfographComponent', () => {
  let component: InfographComponent;
  let fixture: ComponentFixture<InfographComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfographComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfographComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

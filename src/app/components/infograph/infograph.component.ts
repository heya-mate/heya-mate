import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-infograph',
  templateUrl: './infograph.component.html',
  styleUrls: ['./infograph.component.sass']
})
export class InfographComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
    $('.owl-carousel').owlCarousel({
      items:1,
      loop:true,
      autoplay: true,
      autoplayTimeout:4000,
      autoplayHoverPause:false,
      smartSpeed:450
  });

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HpVisualizeParentComponent } from './hp-visualize-parent.component';

describe('HpVisualizeParentComponent', () => {
  let component: HpVisualizeParentComponent;
  let fixture: ComponentFixture<HpVisualizeParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HpVisualizeParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HpVisualizeParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

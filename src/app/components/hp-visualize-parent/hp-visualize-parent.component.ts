import { Component, OnInit , ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VisualizeDialogComponent} from '../../homepage-visualize/visualize-dialog/visualize-dialog.component';
import {MatDialog} from '@angular/material';
import { PreferenceService } from '../../services/preference.service';

@Component({
  selector: 'app-hp-visualize-parent',
  templateUrl: './hp-visualize-parent.component.html',
  styleUrls: ['./hp-visualize-parent.component.sass'],
  encapsulation: ViewEncapsulation.None,
})
export class HpVisualizeParentComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  country: string;
  v1Parameter: string;
  gender: string;
  v4Parameter: string;

  visualisationUrl: string[] = ['https://public.tableau.com/views/CulturalDiversity-IncomingPeople/Dashboard1?:embed=y&:showVizHome=no&:display_count=yes&publish=yes&:origin=viz_share_link',
  'https://public.tableau.com/views/InternationalstudentscomingtoAustralia-2011to2019greaterthan50/Dashboard1?:embed=y&:display_count=yes&:showVizHome=no&publish=yes&:origin=viz_share_link',
  'https://public.tableau.com/views/NationalSurveyDashboard/Dashboard1?:embed=y&:display_count=yes&:showVizHome=no&publish=yes&:origin=viz_share_link',
  'https://public.tableau.com/views/MentalHealthDashboard_15669838260980/Dashboard1?:embed=y&:display_count=yes&:showVizHome=no&publish=yes&:origin=viz_share_link',
  'https://public.tableau.com/views/ConsumerPerspectivesDashboard/Dashboard1?:embed=y&:showVizHome=no&:display_count=yes&publish=yes&:origin=viz_share_link',
  'https://public.tableau.com/views/CulturalDiversity-EnglishSpeaking/Dashboard1?:embed=y&:showVizHome=no&:display_count=yes&publish=yes&:origin=viz_share_link'
  ];

visualisationTitle: string[] = ['Number of people immigrating to Australia',
'International students coming to Australia',
'Common Mental Health issues in last decade',
'Services taken by people aged 18 to 24',
'Recent ratings for Mental Health Services\n(1: lowest to 5: best)',
  'English Speaking Status of People Arriving in Australia(1991-2016)'
];


    constructor(private _formBuilder: FormBuilder, public dialog: MatDialog,public preferenceService: PreferenceService) {
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });
    this.sixthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });


  }

  openDialog(): void {
    this.preferenceService.currentCountry.subscribe(country => this.country = country);
    if (typeof this.country === "undefined") {
      this.v1Parameter = "";
    } else {
        this.v1Parameter = this.country;
    }
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[0] + "&Country of Birth=" + this.v1Parameter,
      visTitle: this.visualisationTitle[0]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogTwo(): void {
    this.preferenceService.currentCountry.subscribe(country => this.country = country);
    if (typeof this.country === "undefined") {
      this.v1Parameter = "";
    } else {
      this.v1Parameter = this.country;
    }
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[1] + "&Nationality=" + this.v1Parameter,
      visTitle: this.visualisationTitle[1]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogThree(): void {
    this.preferenceService.currentGender.subscribe(gender => this.gender = gender);
    if (typeof this.gender === "undefined") {
      this.v4Parameter = "";
    } else {
      this.v4Parameter = this.gender;
    }
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[2]  + `&Gender=${this.v4Parameter}`,
      visTitle: this.visualisationTitle[2]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogFour(): void {
    this.preferenceService.currentGender.subscribe(gender => this.gender = gender);
    if (typeof this.gender === "undefined") {
      this.v4Parameter = "";
    } else {
      this.v4Parameter = this.gender;
    }
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[3] + `&Gender=${this.v4Parameter}`,
      visTitle: this.visualisationTitle[3]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogFive(): void {
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[4],
      visTitle: this.visualisationTitle[4]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogSix(): void {
    const  dialogRef = this.dialog.open(VisualizeDialogComponent, {data: {visLink: this.visualisationUrl[5],
        visTitle: this.visualisationTitle[5]}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}

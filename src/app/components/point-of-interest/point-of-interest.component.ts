import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { PreferenceService } from '../../services/preference.service';
declare var $: any;

@Component({
  selector: 'app-point-of-interest',
  templateUrl: './point-of-interest.component.html',
  styleUrls: ['./point-of-interest.component.sass']
})
export class PointOfInterestComponent implements OnInit {

  public items: any[] = [];
  nationality: string;
  poiParameter: string;
  showProgressSnipper = true;

  constructor(public http: HttpClient) { }

  ngOnInit() {
    $(document).ready(function() {
      $('#toHome').show();
      $('#toPalette').hide();

});

    this.onFetchPointOfInterest();
  }

  async onFetchPointOfInterest() {
    this.http.get('https://heya-mate.ml/api/point_of_interest/melbourne+city+attractions').subscribe((response) => {
      const result = response;
      console.log(response);
      let numOfInterest = 0;
      for (let entry of Object.values(result)) {
        if (entry.name.length > 30) {
          entry.name = entry.name.substring(0, 20) + '...';
        }
        if (entry.formatted_address.length > 50) {
          entry.formatted_address = entry.formatted_address.substring(0, 50) + '...';
        }
        this.items.push({
              "name" : entry.name,
              "rating" : entry.rating,
              "address" : entry.formatted_address,
              "photo_reference" : entry.photo_reference,
              "url" : entry.url
            });
        numOfInterest ++;
        if (numOfInterest === 50) {
          break;
        }
      }
      this.showProgressSnipper = false;
    });
  }

  async onSearchPOI(inputCountry: HTMLInputElement) {
    this.showProgressSnipper = true;
    if (inputCountry.value !== undefined) {
      this.poiParameter = inputCountry.value + '+attractions+near+melbourne+australia';
      this.items = [];
      this.http.get(`https://heya-mate.ml/api/point_of_interest/${this.poiParameter}`).subscribe((response) => {
        const result = response;
        let numOfInterest = 0;
        for (let entry of Object.values(result)) {
          if (entry.formatted_address.length > 30) {
            entry.formatted_address = entry.formatted_address.substring(0, 30) + '...';
          }
          this.items.push({
            "name" : entry.name,
            "rating" : entry.rating,
            "address" : entry.formatted_address,
            "photo_reference" : entry.photo_reference,
            "url" : entry.url
          });
          numOfInterest ++;
          if (numOfInterest === 50) {
            break;
          }
        }
        this.showProgressSnipper = false;
      });
    }
  }
}

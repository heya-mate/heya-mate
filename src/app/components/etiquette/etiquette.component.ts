import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-etiquette',
  templateUrl: './etiquette.component.html',
  styleUrls: ['./etiquette.component.sass']
})
export class EtiquetteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(document).ready(function() {
      $('#toHome').show();
      $('#toPalette').hide();

});
  }

}

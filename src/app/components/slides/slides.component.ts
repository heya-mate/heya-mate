import { Component, OnInit,OnChanges } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { PreferenceComponent } from '../preference/preference.component';
import { PreferenceService } from '../../services/preference.service';
import { Router } from "@angular/router";
declare var $: any;

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.sass']
})

export class SlidesComponent implements OnInit{

  country: string;
  gender: string;
  issue: string;
  university: string;
  badgeCount: number;
  badgeHidden: boolean = false;

  flag = true; // true: hide popup. false: display popup.

  constructor( private router: Router,public dialog: MatDialog,      public preferenceService: PreferenceService) { }


  ngOnInit() {

    $('.owl-carousel').owlCarousel({
      animateOut: 'slideOutDown',
      animateIn: 'flipInX',      
      items:1,
      loop:true,
      autoplay: true,
      autoplayTimeout:2500,
      autoplayHoverPause:false,
      smartSpeed:450
  });

  $('.t1').css('marginTop', $('.t1').height()/1.2+'pt');

    this.preferenceService.currentCountry.subscribe(country => this.country = country);
    this.preferenceService.currentGender.subscribe(gender => this.gender = gender);
    this.preferenceService.currentIssue.subscribe(issue => this.issue = issue);
    this.preferenceService.currentUniversity.subscribe(university => this.university = university);

    if (this.country.length > 0){this.badgeCount++}
    if (this.gender.length > 0){this.badgeCount++}
    if (this.issue.length > 0){this.badgeCount++}
    if (this.university.length > 0){this.badgeCount++}
    if(this.badgeCount > 0){this.badgeHidden = false}else{this.badgeHidden = true}

  }

  display() {
    this.flag = false;
  }

  hidden() {
    this.flag = true;
  }

  openPreference(): void {

    const dialogRef = this.dialog.open(PreferenceComponent ,{
      hasBackdrop: true,
      ariaDescribedBy: "preference",
    });


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}

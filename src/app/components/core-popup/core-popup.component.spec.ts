import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorePopupComponent } from './core-popup.component';

describe('CorePopupComponent', () => {
  let component: CorePopupComponent;
  let fixture: ComponentFixture<CorePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

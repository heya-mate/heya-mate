import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { CorePopupService } from '../../services/core-popup.service';
import { FormGroup, FormBuilder } from '@angular/forms';


export interface CorePopupData {
  cultureshock: boolean;
  homesick: boolean;
  loneanx: boolean;
}

@Component({
  selector: 'app-core-popup',
  templateUrl: './core-popup.component.html',
  styleUrls: ['./core-popup.component.sass']
})

export class CorePopupComponent implements OnInit {

  cultureshock: boolean;
  homesick: boolean;
  loneanx: boolean;
  csFuncOne: boolean;
  csFuncTwo: boolean;
  csFuncThree: boolean;
  hsFuncOne: boolean;
  hsFuncTwo: boolean;
  hsFuncThree: boolean;
  laFuncOne: boolean;
  laFuncTwo: boolean;
  laFuncThree: boolean;
  csPanel: boolean;
  hsPanel: boolean;
  laPanel: boolean;
  isClose: boolean;

  constructor(public fb: FormBuilder,
    public dialogRef: MatDialogRef<CorePopupComponent>,
    public corePopupService: CorePopupService,
    @Inject(MAT_DIALOG_DATA) public data: CorePopupData) { }


  ngOnInit() {

    this.corePopupService.currentCultureShock.subscribe(cultureshock => this.cultureshock = cultureshock);
    this.corePopupService.currentHomeSick.subscribe(homesick => this.homesick = homesick);
    this.corePopupService.currentLoneAnx.subscribe(loneanx => this.loneanx = loneanx);
    
    if (this.cultureshock){
      this.csPanel = true
      this.hsPanel = false
      this.laPanel = false
      this.isClose = true
    }

    if (this.homesick){
      this.csPanel = false
      this.hsPanel = true
      this.laPanel = false
      this.isClose = true      
    }
/*
    if (this.loneanx){
      this.csPanel = false
      this.hsPanel = false
      this.hsPanel = true
      this.isClose = true      
    }*/
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  clickCSPanel() {
    this.csFuncOne = false;
    this.csFuncTwo = false;        
    this.csFuncThree = false;
    this.csPanel = true;
    this.isClose = true    
  }

  clickHSPanel() {
    this.hsFuncOne = false;
    this.hsFuncTwo = false;        
    this.hsFuncThree = false;
    this.hsPanel = true;
    this.isClose = true    
  } 
  /*
  clickLAPanel() {
    this.laFuncOne = false;
    this.laFuncTwo = false;        
    this.laFuncThree = false;
    this.laPanel = true;
    this.isClose = true    
  }    
*/
  clickCSFuncOne() {
    this.csFuncOne = true;
    this.csPanel = false;
    this.isClose = false
  }

  clickCSFuncTwo() {
    this.csFuncTwo = true;
    this.csPanel = false;
    this.isClose = false    
  }

  clickCSFuncThree() {
    this.csFuncThree = true;
    this.csPanel = false;
    this.isClose = false    
  }  

  clickHSFuncOne() {
    this.hsFuncOne = true;
    this.hsPanel = false;
    this.isClose = false    
  }

  clickHSFuncTwo() {
    this.hsFuncTwo = true;
    this.hsPanel = false;
    this.isClose = false    
  }
/*
  clickHSFuncThree() {
    this.hsFuncThree = true;
    this.hsPanel = false;
    this.isClose = false    
  }  

  clickLAFuncOne() {
    this.laFuncOne = true;
    this.laPanel = false;
    this.isClose = false    
  }

  clickLAFuncTwo() {
    this.laFuncTwo = true;
    this.laPanel = false;
    this.isClose = false    
  }

  clickLAFuncThree() {
    this.laFuncThree = true;
    this.laPanel = false;
    this.isClose = false    
  }  
*/
  onCloseClick(): void {
    this.dialogRef.close();
  }

}
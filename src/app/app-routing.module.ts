import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FeaturesEntryComponent } from "./components/features-entry/features-entry.component";
import { SuburbsComponent } from "./components/suburbs/suburbs.component";
import { SlangComponent } from "./components/slang/slang.component";
import { PointOfInterestComponent } from "./components/point-of-interest/point-of-interest.component";
import { RestaurantsComponent } from "./components/restaurants/restaurants.component";
import { FestivalsComponent } from "./components/festivals/festivals.component";
import { EventsComponent } from "./components/events/events.component";
import { MeetupComponent } from "./components/meetup/meetup.component";
import { HomeComponent } from "./components/home/home.component";
import { EtiquetteComponent } from "./components/etiquette/etiquette.component";
import { NotFoundComponent } from "./not-found/not-found.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    component: HomeComponent,
    data: {animation: 'Home'}
  },
  {
    path: "features",
    component: FeaturesEntryComponent,
    data: {animation: 'Other'}
  },
  {
    path: "accommodation",

    component: SuburbsComponent,
    data: {animation: 'Other'}
  },
  {
    path: "aussie_slang",
    component: SlangComponent,
    data: {animation: 'Other'}
  },
  {
    path: "local_attractions",
    component: PointOfInterestComponent,
    data: {animation: 'Other'}
  },
  {
    path: "food",
    component: RestaurantsComponent,
    data: {animation: 'Other'}
  },
  {
    path: "festivals",
    component: FestivalsComponent,
    data: {animation: 'Other'}
  },
  {
    path: "events",
    component: EventsComponent,
    data: {animation: 'Other'}
  },
  {
    path: "meetups",
    component: MeetupComponent,
    data: {animation: 'Other'}
  },
  {
    path: "culture_and_etiquette",
    component: EtiquetteComponent,
    data: {animation: 'Other'}
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

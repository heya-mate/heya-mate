import { TestBed } from '@angular/core/testing';

import { CorePopupService } from './core-popup.service';

describe('CorePopupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorePopupService = TestBed.get(CorePopupService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorePopupService {

  private controlCultureShock = new BehaviorSubject(false);
  private controlHomeSick = new BehaviorSubject(false);
  private controlLoneAnx = new BehaviorSubject(false);

  constructor() { }

  currentCultureShock = this.controlCultureShock.asObservable();
  currentHomeSick = this.controlHomeSick.asObservable();
  currentLoneAnx = this.controlLoneAnx.asObservable();


  setControlCultureShock(cultureshock: boolean) {
    return this.controlCultureShock.next(cultureshock);
  }

  setControlHomeSick(homesick: boolean) {
    return this.controlHomeSick.next(homesick);
  }

  setControlLoneAnx(loneanx: boolean) {
    return this.controlLoneAnx.next(loneanx);
  }

}

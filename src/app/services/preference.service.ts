import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreferenceService {

  private controlCountry = new BehaviorSubject("");
  private controlGender = new BehaviorSubject("");
  private controlssue = new BehaviorSubject("");
  private controlUniversity = new BehaviorSubject("");

  constructor() { }

  currentCountry = this.controlCountry.asObservable();
  currentGender = this.controlGender.asObservable();
  currentIssue = this.controlssue.asObservable();
  currentUniversity = this.controlUniversity.asObservable();

  setControlCountry(country: string) {
    return this.controlCountry.next(country);
  }

  setControlGender(gender: string) {
    return this.controlGender.next(gender);
  }

  setControlIssue(issue: string) {
    return this.controlssue.next(issue);
  }

  setControlUniversity(university: string) {
    return this.controlUniversity.next(university);
  }
}

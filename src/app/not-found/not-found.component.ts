import { Component, OnInit } from "@angular/core";
declare var $: any;

@Component({
  selector: "app-not-found",
  templateUrl: "./not-found.component.html",
  styleUrls: ["./not-found.component.css"]
})
export class NotFoundComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $(document).ready(function() {
      function doAnimation() {
        $(".bounce").effect(
          "bounce",
          { times: 3, distance: 20 },
          5000,
          doAnimation
        );
      }

      doAnimation();

      $("#toHome").hide();
      $("#toPalette").hide();
      $("#toTop").hide();
    });
  }
}

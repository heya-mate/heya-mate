import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { BannerComponent} from './components/banner/banner.component';
import { FooterComponent} from './components/footer/footer.component';
import { PreferenceComponent} from './components/preference/preference.component';
import { ExternalResourcesComponent } from './components/external-resources/external-resources.component';
import { SlidesComponent } from './components/slides/slides.component';
import { HpVisualizeParentComponent } from './components/hp-visualize-parent/hp-visualize-parent.component';
import { HttpClientModule } from '@angular/common/http';
import { VSixComponent} from './homepage-visualize/v-six/v-six.component';
import { MatDialogModule, MatToolbarModule} from '@angular/material';
import { VisualizeDialogComponent} from './homepage-visualize/visualize-dialog/visualize-dialog.component';
import { PointOfInterestComponent } from './components/point-of-interest/point-of-interest.component';
import { RestaurantsComponent} from './components/restaurants/restaurants.component';
import { CoreFunctionsComponent} from './components/core-functions/core-functions.component';
import { CorePopupComponent} from './components/core-popup/core-popup.component';
import { EventsComponent} from './components/events/events.component';
import { SlangComponent} from './components/slang/slang.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FestivalsComponent } from './components/festivals/festivals.component';
import { SuburbsComponent} from './components/suburbs/suburbs.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { HomeComponent } from './components/home/home.component';
import { IntroductionComponent} from './components/introduction/introduction.component';
import { FeaturesEntryComponent} from './components/features-entry/features-entry.component';
import { VisualizationsComponent} from './components/visualizations/visualizations.component';
import { MeetupComponent} from './components/meetup/meetup.component';
import { InfographComponent} from './components/infograph/infograph.component';
import { EtiquetteComponent} from './components/etiquette/etiquette.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NotFoundComponent} from "./not-found/not-found.component";

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    FooterComponent,
    PreferenceComponent,
    ExternalResourcesComponent,
    SlidesComponent,
    HpVisualizeParentComponent,
    VSixComponent,
    VisualizeDialogComponent,
    PointOfInterestComponent,
    RestaurantsComponent,
    CoreFunctionsComponent,
    CorePopupComponent,
    EventsComponent,
    SlangComponent,
    FestivalsComponent,
    SuburbsComponent,
    HomePageComponent,
    HomeComponent,
    IntroductionComponent,
    FeaturesEntryComponent,
    VisualizationsComponent,
    MeetupComponent,InfographComponent,EtiquetteComponent,
    NotFoundComponent
  ],
  exports:[ SlidesComponent ],
  imports: [
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    MatDialogModule,
    MatToolbarModule,
    NgbModule,
    MatProgressSpinnerModule
  ],
  entryComponents: [
    VisualizeDialogComponent
    , PreferenceComponent
    , CorePopupComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule {  }

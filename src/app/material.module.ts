// material.module.ts

import { NgModule } from '@angular/core';

import { MatPaginatorModule, MatTableModule,MatRadioModule,MatBadgeModule,MatDividerModule,MatCardModule,MatMenuModule,MatStepperModule, MatTooltipModule, MatIconModule, MatListModule, MatSidenavModule, MatCheckboxModule, MatSliderModule, MatSelectModule, MatOptionModule, MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule } from '@angular/material';

@NgModule({
  exports: [ MatPaginatorModule,MatTableModule,MatRadioModule,MatBadgeModule,MatFormFieldModule,MatDividerModule,MatCardModule,MatMenuModule,MatStepperModule,MatTooltipModule, MatIconModule, MatListModule, MatSidenavModule, MatCheckboxModule, MatSliderModule, MatSelectModule, MatOptionModule, MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule]
})
export class MaterialModule { }